import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JTextField;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeoutException;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class Main extends JFrame implements MqttCallback  {

	private JPanel contentPane;
	private JTextField medida;
	private final static String QUEUE_NAME = "hello";
	private int medidaInt;
	private MqttClient client, clientReceive;
	private boolean ligado = true;
	private String ip = "tcp://52.67.19.49:1883";
	private String idSimulador = "Pluvio 2";


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Main() {
		setTitle("Simulador Pluvi\u00F4metro");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Simulador Pluvi\u00F4metro 2");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 16));
		lblNewLabel.setBounds(22, 11, 273, 14);
		contentPane.add(lblNewLabel);
		
		JButton btnEnviarMedida = new JButton("Enviar medida");
		btnEnviarMedida.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Enviando: "+ medida.getText().toString());
				if(ligado){
					try{
						medidaInt = Integer.parseInt(medida.getText().toString());
						System.out.println(medidaInt);
						client = new MqttClient(ip, "Sending");
				        client.connect();
				        //client.setCallback(this);
				        client.subscribe("simulador");
				        String msg = idSimulador+"---"+ medidaInt;
				        MqttMessage message = new MqttMessage();
				        message.setPayload(msg
				                .getBytes());
				        client.publish("simulador", message);
					    				    
					    JOptionPane.showMessageDialog(null, "Enviado com Sucesso!", "ATEN��O!", JOptionPane.INFORMATION_MESSAGE);
						
					}catch (NumberFormatException e1){
						
						JOptionPane.showMessageDialog(null, "O dado informado n�o � um n�mero!", "ATEN��O!", JOptionPane.ERROR_MESSAGE);
	
					}catch (MqttException e2) {
				        e2.printStackTrace();
				        JOptionPane.showMessageDialog(null, "Erro ao enviar dado!", "ATEN��O!", JOptionPane.ERROR_MESSAGE);
	
				    }
				} else{
					JOptionPane.showMessageDialog(null, "O simulador foi bloqueado remotamente!", "ATEN��O", JOptionPane.ERROR_MESSAGE);
				}
				 
			}
		});
		btnEnviarMedida.setBounds(22, 126, 169, 23);
		contentPane.add(btnEnviarMedida);
		
		medida = new JTextField();
		medida.setBounds(23, 78, 86, 20);
		contentPane.add(medida);
		medida.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("mm");
		lblNewLabel_1.setFont(new Font("Arial", Font.PLAIN, 13));
		lblNewLabel_1.setBounds(119, 81, 46, 14);
		contentPane.add(lblNewLabel_1);
		Timer timer = new Timer(); 
		timer.schedule( new TimerTask() 
		{ 
			public void run() { 
				
				try {
					clientReceive = new MqttClient(ip, "Receive");
					MqttConnectOptions connOpts = new MqttConnectOptions();
					connOpts.setCleanSession(true);
					clientReceive.connect(connOpts);
					clientReceive.subscribe("estado2");
					clientReceive.setCallback(new MqttCallback() {
						 
					    public void messageArrived(String topic, MqttMessage message) throws Exception {
					        //String time = new Timestamp(System.currentTimeMillis()).toString();
					        System.out.println("\nReceived a Message!" +
					            "\n\tTopic:   " + topic +
					            "\n\tMessage: " + new String(message.getPayload()) +
					            "\n\tQoS:     " + message.getQos() + "\n");
					        //latch.countDown(); // unblock main thread
					        String msg = new String(message.getPayload());
					        if(msg.equals("Desligar")){
					        	System.out.println("Desligando Simulador!");
					        	ligado = false;
					        }
					        
					        if(msg.equals("Ligar")){
					        	System.out.println("Ligando Simulador!");
					        	ligado = true;
					        }
					    }
					 
					    public void connectionLost(Throwable cause) {
					       // System.out.println("Connection to Solace broker lost!" + cause.getMessage());
					        //latch.countDown();
					    }
					 
					    public void deliveryComplete(IMqttDeliveryToken token) {
					    }
					 
					});
				} catch (MqttException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} 
		}, 0, 1000);
	}
	
	

	@Override
	public void connectionLost(Throwable cause) {
	    // TODO Auto-generated method stub

	}

	@Override
	public void messageArrived(String topic, MqttMessage message)
	        throws Exception {
	 System.out.println(message);   
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
	    // TODO Auto-generated method stub

	}
}
